//
//  GameScene.swift
//  Gravity
//
//  Created by Bradley Anderson on 3/11/15.
//  Copyright (c) 2015 Bradley Anderson. All rights reserved.
//

import SpriteKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    override func didMoveToView(view: SKView) {
        
        
        let borderBody = SKPhysicsBody (edgeLoopFromRect: self.frame)
        
        borderBody.friction = 0
        self.physicsBody = borderBody
        
        
        
        //self.physicsBody?.categoryBitMask = SceneEdgeCategory
        //self.physicsBody?.friction = 0.2
        //self.physicsBody?.restitution = 0.5
        
  }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        /* Called when a touch begins */
        
        for touch: AnyObject in touches {
            let location = touch.locationInNode(self)
            
            let circle = SKShapeNode(circleOfRadius: 40)
            
            circle.position = location
            circle.strokeColor = UIColor.blackColor()
            
            circle.fillColor = UIColor.greenColor()
            
            let text = SKLabelNode(text: String("BALL"))
            // we set the font
            text.fontName = "Helvetica"
            text.fontSize = 14.0
            text.fontColor = UIColor.blackColor()
            
            // we nest the text label in our ball
            circle.addChild(text)
            
            circle.physicsBody = SKPhysicsBody(circleOfRadius: 40)
            circle.physicsBody?.affectedByGravity = true
            
            // this defines the roughness of the ball
            circle.physicsBody?.friction = 0.1
            //restitution is bounciness, 1 = perfectly elastic
            circle.physicsBody?.restitution = 1
            //mass is arbitrary and relative
            circle.physicsBody?.mass = 0.5
            // this will allow the balls to rotate when bouncing off each other
            circle.physicsBody?.allowsRotation = true
            
            circle.physicsBody?.applyImpulse(CGVectorMake(0, 100))
            
            self.addChild(circle)
            
        }
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
